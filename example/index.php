<?php

require_once(__DIR__ . '/vendor/autoload.php');

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

/**
Used to implement aspect oriented programming. This allows cross script functionality to be injected
based on annotation point references.

In reality we only use it for logging, as anything else becomes a bit more magicy and difficult to follow
*/
$aspectKernel = Sixdg\DynamicsCRMConnector\AspectKernel\ApplicationAspectKernel::getInstance();
$aspectKernel->init(
    [
        'cacheDir'     => null, //Place this in a directory if you want aspects cached
        'includePaths' => [
            __DIR__ . '/vendor/sixdg/'
        ],
        'debug'        => false
    ]
);

$logger = new Logger('crm_connector');
$logger->pushHandler(new StreamHandler('log.txt'));

$config = [
    'username'     => 'domain\user',
    'password'     => 'Pa55w0rd',
    'crm'          => 'https://crm:444/',
    'adfs'         => 'https://sts1.crm.co.uk/adfs/',
    'organization' => 'Testing',
    'discoveryUrl' => 'XRMServices/2011/Discovery.svc'
];

$connector = new \Sixdg\DynamicsCRMConnector\DynamicsCRMConnector($config, $aspectKernel, $logger);

$controller = $connector->getController();

$entityFactory = $connector->getEntityFactory();
$account = $entityFactory->makeEntity('account');

var_dump($account);
