<?php
require_once(__DIR__ . '/../../vendor/autoload.php');

use Monolog\Handler\RotatingFileHandler;
use Monolog\Logger;
use Sixdg\DynamicsCRMConnector\AspectKernel\ApplicationAspectKernel;

$aspectKernel = ApplicationAspectKernel::getInstance();
$aspectKernel->init(
    [
        'cacheDir'     => null,
        'includePaths' => [
            __DIR__ . '/../../src/sixdg/Sixdg/DynamicsCRMConnector'
        ],
        'debug'        => false
    ]
);

$logger = new Logger('DynamicsTesting-testing');
$logger->pushHandler(new RotatingFileHandler(__DIR__.'/../../logs/log', 10));

$GLOBALS['logger'] = $logger;
$GLOBALS['aspectKernel'] = $aspectKernel;
$GLOBALS['config'] = require_once('config.php');
$GLOBALS['faker'] = Faker\Factory::create('en_GB');
\Mockery::getConfiguration()->allowMockingNonExistentMethods(false);
