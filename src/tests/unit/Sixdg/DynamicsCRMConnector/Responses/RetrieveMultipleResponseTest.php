<?php

use Sixdg\DynamicsCRMConnector\Responses\RetrieveMultipleResponse;
use Sixdg\DynamicsCRMConnector\Test\BaseTest;

/**
 * @author Leandro Miranda
 * @date 24/07/2013
 */
class RetrieveMultipleResponseTest extends BaseTest
{

    /**
     * @var Sixdg\DynamicsCRMConnector\Components\Responses\RetrieveMultipleResponse
     */
    protected $response;

    public function setUp()
    {
        $this->response = new RetrieveMultipleResponse();
        $domHelper = new \DOMDocument();
        $domHelper->load(__DIR__ . '/Fixtures/retrieveMultipleResponse.xml');
        $this->response->loadXML($domHelper->saveXML());
    }

    public function testGetPagingCookie()
    {
        $cookie = $this->response->getPagingCookie(false);
        $this->assertEquals('DOMElement', get_class($cookie));
        $stringCookie = $this->response->getPagingCookie(true);
        $this->assertEquals('<cookie page="1"><contactid last="{0A80F4F6-4AEA-E211-90C3-005056881769}" first="{0A80F4F6-4AEA-E211-90C3-005056881769}" /></cookie>', $stringCookie);
    }

    public function testGetPageNumber()
    {
        $pageNumber = $this->response->getPageNumber();
        $this->assertEquals(1, $pageNumber);
    }

    public function testGetEntityName()
    {
        $entityName = $this->response->getEntityName();
        $this->assertEquals('contact', $entityName);
    }

    public function testGetMoreRecords()
    {
        $value = $this->response->getMoreRecords();
        $this->assertEquals('false', $value);
    }

    public function testAsArray()
    {
        $value = $this->response->asArray();
        $this->assertTrue(is_array($value));
        $this->assertEquals(1, count($value));
    }

    public function testGetEntities()
    {
        $entityFactory = $this->getEntityFactory('contact');

        $entities = $this->response->getEntities($entityFactory);

        $this->assertTrue(is_array($entities));
        $this->assertEquals(count($entities), 1);
        $this->assertEquals(get_class($entities[0]), 'Sixdg\DynamicsCRMConnector\Models\Entity');
    }
}
