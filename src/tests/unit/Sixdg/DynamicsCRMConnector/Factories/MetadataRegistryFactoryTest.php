<?php
/**
 * Created by JetBrains PhpStorm.
 * User: alan.hollis
 * Date: 12/08/13
 * Time: 10:45
 */

class MetadataRegistryFactoryTest extends PHPUnit_Framework_TestCase
{
    protected $factory = null;

    public function setUp()
    {
        $this->controller = Mockery::mock('\Sixdg\DynamicsCRMConnector\Controllers\DynamicsCRMController');
    }

    private function makeEntity()
    {
        $reflection = new \ReflectionClass('\Sixdg\DynamicsCRMConnector\Models\Entity');
        $entity = $reflection->newInstanceWithoutConstructor();
        $entity->setEntityName('account');

        return $entity;
    }

    public function testReturnsArrayOfMetaDataItems()
    {
        $this->controller->shouldReceive('findMetadata')->withAnyArgs()->andReturn(
            file_get_contents(__DIR__ . '/../Fixtures/accountMetadata.xml')
        );

        $this->controller->shouldReceive('getCache')->andReturn(null);

        $this->factory = new \Sixdg\DynamicsCRMConnector\Factories\MetadataRegistryFactory($this->controller);
        $response = $this->factory->makeMetaData($this->makeEntity());

        $this->assertTrue(is_array($response));
        $this->assertTrue(
            array_pop($response) instanceof
            \Sixdg\DynamicsCRMConnector\Models\EntityMetadataRegistryItem
        );
    }

    public function testInternalCache()
    {
        $this->controller->shouldReceive('findMetadata')->withAnyArgs()->once()->andReturn(
            file_get_contents(__DIR__ . '/../Fixtures/accountMetadata.xml')
        );
        $this->controller->shouldReceive('getCache')->andReturn(null);

        $entity = $this->makeEntity();
        $this->factory = new \Sixdg\DynamicsCRMConnector\Factories\MetadataRegistryFactory($this->controller);
        $this->factory->makeMetaData($entity);
        //Should not cause mockery to throw error
        $this->factory->makeMetaData($entity);
    }

    public function testExternalCache()
    {
        \Mockery::getConfiguration()->allowMockingNonExistentMethods(true);

        $key = '6dgcrm-accountMetaData';

        $cache = \Mockery::mock('FakeCache');
        $cache->shouldReceive('set')->once();
        $cache->shouldReceive('exists')->with($key)->andReturn(false, true);
        $cache->shouldReceive('getKeyPrefix')->andReturn('6dgcrm');
        $cache->shouldReceive('get')->andReturn(
            file_get_contents(__DIR__ . '/../Fixtures/accountMetadata.xml')
        );

        $this->controller->shouldReceive('findMetadata')->withAnyArgs()->once()->andReturn(
            file_get_contents(__DIR__ . '/../Fixtures/accountMetadata.xml')
        );

        $this->controller->shouldReceive('getCache')->andReturn($cache);
        $this->factory = new \Sixdg\DynamicsCRMConnector\Factories\MetadataRegistryFactory($this->controller);
        $entity = $this->makeEntity();
        $this->factory->makeMetaData($entity);
        //Should not cause mockery to throw error
        $this->factory->makeMetaData($entity);

        \Mockery::getConfiguration()->allowMockingNonExistentMethods(false);
    }
}
