<?php

use Sixdg\DynamicsCRMConnector\Test\BaseTest;

/**
 * Created by JetBrains PhpStorm.
 * User: alan.hollis
 * Date: 04/07/13
 * Time: 13:17
 */

/**
 * Class RetrieveRequestTest
 */
class RetrieveRequestTest extends BaseTest
{
    /**
     * @var RequestBuilder
     */
    protected $requestBuilder;

    protected $entityRequest;

    public function setUp()
    {
        $this->requestBuilder = $this->getAndBootstrapRequestBuilder();
    }
    public function testGetEntityRequest()
    {
        $request = $this->requestBuilder->getRequest('RetrieveRequest');

        $request->setEntityName('systemuser');
        $request->setEntityId('9e1bff06-f7c4-e111-88c9-0050568d005a');

        $xml = $request->getXML();

        $doc = new \DOMDocument;
        $doc->loadXML($xml);
        $doc->preserveWhiteSpace = false;
        $doc->formatOutput = true;

        $expected = new \DOMDocument;
        $expected->loadXML(file_get_contents(__DIR__ . '/Fixtures/SingleEntityRequest.xml'));
        $expected->preserveWhiteSpace = false;
        $expected->formatOutput = true;

        $this->assertEquals($expected->saveXML(), $doc->saveXML());
    }
}
