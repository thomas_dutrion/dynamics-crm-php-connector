<?php

namespace Sixdg\DynamicsCRMConnector\Test;

use Sixdg\DynamicsCRMConnector\Builders\RequestBuilder;
use Sixdg\DynamicsCRMConnector\Services\EntityToDomConverter;
use Sixdg\DynamicsCRMConnector\Components\DOM\DOMHelper;

/**
 * Base class to hold shared logic for testing
 *
 * @author Leandro Miranda
 */
abstract class BaseTest extends \PHPUnit_Framework_TestCase
{

    protected $entityFactory;
    /**
     *
     * @return array
     */
    protected function createEntityMetaData()
    {
        $metaData = [];

        $metaData['address1_city'] = $this->createEntityMetaDataItem('address1_city');
        $metaData['statecode'] = $this->createEntityMetaDataItem('statecode');
        $metaData['customertypecode'] = $this->createEntityMetaDataItem('customertypecode');
        $metaData['address2_addresstypecode'] = $this->createEntityMetaDataItem('address2_addresstypecode');

        return $metaData;
    }

    /**
     *
     * @param  string                                                        $logicalName
     * @return \Sixdg\DynamicsCRMConnector\Models\EntityMetadataRegistryItem
     */
    protected function createEntityMetaDataItem($logicalName)
    {
        $metaDataItem = new \Sixdg\DynamicsCRMConnector\Models\EntityMetadataRegistryItem();
        $metaDataItem->setLogicalName($logicalName);

        return $metaDataItem;
    }

    /**
     * Returns an instance of RequestBuilder
     *
     */
    protected function getAndBootstrapRequestBuilder()
    {
        $domHelper = new DOMHelper();
        $timeHelper = \Mockery::mock('Sixdg\DynamicsCRMConnector\Components\Time\TimeHelper');
        $timeHelper->shouldReceive('getCurrentTime')->andReturn('2013-07-04T10:34:51.00Z');
        $timeHelper->shouldReceive('getExpiryTime')->andReturn('2013-07-04T10:35:51.00Z');

        $entityToDomConverter = new EntityToDomConverter($domHelper);
        $requestBuilder = new RequestBuilder($domHelper, $timeHelper, $entityToDomConverter);
        $requestBuilder->setSecurityToken(
            [
                'securityToken' => '<MockSecurityTokenResponse></MockSecurityTokenResponse>',
                'binarySecret' => '2Y/D9fcqP6YIE/NEMN6h0+u/mMShjqy6P6HC9C4cxPo=',
                'keyIdentifier' => ' _18529f8f-0e27-4dd8-ac82-b34cb54ae302',
            ]
        )
                ->setOrganization('J6testing')
                ->setServer($GLOBALS['config']['crm']);

        return $requestBuilder;
    }

    /**
     * Returns an entity factory which only builds entity given
     * Note that a file containing the entity metaData xml should exist
     * in Fixtures named xxxMetadata.xml where xxx is the entity name.
     *
     * @param string $entityName
     */
    protected function getEntityFactory($entityName)
    {
        if ($this->entityFactory) {
            return $this->entityFactory;
        }
        $this->entityFactory = \Mockery::mock('Sixdg\DynamicsCRMConnector\Factories\EntityFactory');

        $reflection = new \ReflectionClass('\Sixdg\DynamicsCRMConnector\Models\Entity');
        $entity = $reflection->newInstanceWithoutConstructor();

        $entity->setEntityName($entityName);
        $metadataRegistryFactory = new \Sixdg\DynamicsCRMConnector\Factories\MetadataRegistryFactory();

        $dom = new \DOMDocument();
        //load the metadata from file
        $dom->load(__DIR__ . '../../../../../tests/unit/Sixdg/DynamicsCRMConnector/Fixtures/' . $entityName . 'Metadata.xml');
        $metadataRegistry = $metadataRegistryFactory->fetchMetaData($dom->saveXML());
        //set the metadata
        $entity->setMetadataRegistry($metadataRegistry);

        $this->entityFactory->shouldReceive('makeEntity')
                ->andReturn($entity);

        return $this->entityFactory;
    }

    /**
     *
     * @param DOMDocument $expected
     * @param DOMDocument $doc
     */
    protected function assertExpected($expected, $doc)
    {
        $expected->preserveWhiteSpace = false;
        $expected->formatOutput = true;

        /**
         * There's an issue somewhere with formatOutput breaking half way through the xml which is why
         * the fixture xml tags don't align. Further investigation needed.
         */
        $this->assertEquals($expected->saveXML(), $doc->saveXML());
    }
}
