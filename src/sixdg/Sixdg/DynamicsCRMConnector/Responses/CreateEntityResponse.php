<?php
/**
 * Created by JetBrains PhpStorm.
 * User: alan.hollis
 * Date: 02/08/13
 * Time: 15:24
 */
namespace Sixdg\DynamicsCRMConnector\Responses;

/**
 * Class CreateEntityResponse
 *
 * @package Sixdg\DynamicsCRMConnector\Responses
 */
class CreateEntityResponse extends DynamicsCRMResponse
{
    /**
     * @return array
     */
    public function asArray()
    {
        $elements = $this->getElementsByTagName('CreateResult');

        $response = [];
        foreach ($elements as $element) {
            $response[$element->tagName] = $element->nodeValue;
        }

        return $response;
    }

    /**
     *
     * @param \DOMElement $item
     *
     * @return array | string
     */
    protected function extractNodeValue(\DOMElement $item)
    {
        $value = $item->nodeValue;

        return $value;
    }

    /*
     * Returns true if CreateResult tag found / false if tag not found
     *
     * @return bool
     */
    public function isSuccess()
    {
        return (bool) $this->getElementsByTagName('CreateResult')->length;
    }
}
