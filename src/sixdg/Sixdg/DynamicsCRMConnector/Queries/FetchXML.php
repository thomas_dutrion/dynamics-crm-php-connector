<?php
namespace Sixdg\DynamicsCRMConnector\Queries;

/**
 * fetchXML query to be sent to the retrive multiple service
 *
 * @see    http://msdn.microsoft.com/en-us/library/bb930489.aspx
 * @author leandro.miranda
 */
class FetchXML
{

    protected $entityName;
    protected $attribues = array();
    protected $andConditions = array();
    protected $orConditions = array();
    protected $links = array();
    protected $orderAttribute = null;
    protected $orderDescending = false;
    protected $fetchExpression = null;

    /**
     *
     * @param string $name
     *
     * @return \Sixdg\DynamicsCRMConnector\Queries\FetchXML
     */
    public function setEntityName($name)
    {
        $this->entityName = $name;

        return $this;
    }

    /**
     *
     * @param string $attributeName
     *
     * @return \Sixdg\DynamicsCRMConnector\Queries\FetchXML
     */
    public function setOrderAttribute($attributeName)
    {
        $this->orderAttribute = $attributeName;

        return $this;
    }

    /**
     * @param bool $order
     *
     * @return $this
     */
    public function setOrderDescending(bool $order)
    {
        $this->orderDescending = $order;

        return $this;
    }

    /**
     *
     * @param array $attributes
     *
     * @return \Sixdg\DynamicsCRMConnector\Queries\FetchXML
     */
    public function setAttributes(array $attributes)
    {
        $this->attribues = $attributes;

        return $this;
    }

    /**
     *
     * @param array $condition Must have 'attribute', 'operator' and 'value' when required by operator
     *
     * @return \Sixdg\DynamicsCRMConnector\Queries\FetchXML
     */
    public function addAnd(array $condition)
    {
        $this->andConditions[] = $condition;

        return $this;
    }

    /**
     *
     * @param array $condition Must have 'attribute', 'operator' and 'value' when required by operator
     *
     * @return \Sixdg\DynamicsCRMConnector\Queries\FetchXML
     */
    public function addOr(array $condition)
    {
        $this->orConditions[] = $condition;

        return $this;
    }

    /**
     * Return the fetch expression
     *
     * @return string The fetch expression xml
     */
    public function getFetchExpression()
    {
        //if a fetch expresssion was set, return it
        if ($this->fetchExpression) {
            return $this->fetchExpression;
        }

        return $this->buildFetchExpression();
    }

    /**
     * Builds a fetch expression
     *
     * @return string The expression xml
     */
    protected function buildFetchExpression()
    {
        // @codingStandardsIgnoreStart
        $expression = '<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="true">';
        // @codingStandardsIgnoreEnd
        $expression .= $this->getEntityTag();
        $expression .= $this->getFilters();

        //close tags
        $expression .= '</entity></fetch>';

        return $expression;
    }

    /**
     *
     */
    protected function getEntityTag()
    {
        return '<entity name="' . $this->entityName . '">';
    }

    /**
     *
     */
    protected function getAttributesTag()
    {
        if (!$this->attribues) {
            return '<all-attributes/>';
        }
        $attributesTag = '';
        foreach ($this->attribues as $attribute) {
            $attributesTag .= '<attribute name="' . $attribute . '" />';
        }

        return $attributesTag;
    }

    /**
     *
     */
    protected function getFilters()
    {
        $filters = $this->getFilterTag('and', $this->andConditions);
        $filters .= $this->getFilterTag('or', $this->orConditions);

        return $filters;
    }

    /**
     * @param $type
     * @param $conditions
     *
     * @return string
     */
    protected function getFilterTag($type, $conditions)
    {
        if (!$conditions) {
            return '';
        }
        $filter = '<filter type="' . $type . '">';
        foreach ($conditions as $condition) {
            $filter .= '<condition attribute="' . $condition['attribute'] . '" operator="' . $condition['operator'] . '"';
            if (array_key_exists('value', $condition)) {
                $filter .= ' value="' . $this->encodeSpecialChars($condition['value']). '"';
            }
            $filter .= '/>';
        }
        $filter .= '</filter>';

        return $filter;
    }

    /**
     * @param $value
     *
     * @return string
     */
    private function encodeSpecialChars($value)
    {
        $value = str_replace("\r", "\n", $value);
        $value = str_replace("\n\n", "\n", $value);
        $value = str_replace("\n", "&#10;", $value);

        return htmlentities($value);
    }

    /**
     * Set the fetch expression from a xml string
     *
     * @param string $fetchExpressionXML
     *
     * @return \Sixdg\DynamicsCRMConnector\Queries\FetchXML
     */
    public function setFetchExpression($fetchExpressionXML)
    {
        $this->fetchExpression = $fetchExpressionXML;

        return $this;
    }
}
