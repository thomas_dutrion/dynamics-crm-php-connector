Dynamics CRM PHP Connector
=======

Improved API for the Dynamics CRM connector

Installation
------------
Add the library to your project using composer.

Add dependency to your composer.json:

    "sixdg/dynamics-crm-php-connector": "0.1.*"

Basic Setup
------------

Include the composer autoloader:

	require_once(__DIR__ . '/vendor/autoload.php');

Create the aspect kernel

	$aspectKernel = Sixdg\DynamicsCRMConnector\AspectKernel\ApplicationAspectKernel::getInstance();
	$aspectKernel->init(
		[
			'cacheDir'     => null, //Place this in a directory if you want aspects cached
			'includePaths' => [
				__DIR__ . '/vendor/sixdg/'
			],
			'debug'        => false
		]
	);

Create a new PSR-3 logger

	$logger = new Logger('SmpAPI');

Instantiate the connector

	$config = [
		'username'     => 'crm_user',
		'password'     => 'crm_password',
		'crm'          => 'https://pathtocrmserver:444/',
		'adfs'         => 'https://pathtoadfsserver/',
		'organization' => 'crmorganizationtoconnectto'
	];

	$connector = new \Sixdg\DynamicsCRMConnector\DynamicsCRMConnector($config, $aspectKernel, $logger);

Usage
------------

Get the controller

	$controller = $connector->getController();

Create Entity

	$entityFactory = $connector->getEntityFactory();
	$account = $entityFactory->makeEntity('account');
	$account->setName('My account name');

	$controller->create($account);

	$guid = $controller->getLastInsertId();

Fetch Entity

	$account  = $controller->find('account', $guid);

Update Entity

	$account->setName('My updated account name');
	$controller->update($entity);

Delete Entity

	$controller->delete('account', $guid)

Find multiple entities

	$fetchXML = new FetchXML();
	$fetchXML->setFetchExpression(
	    '<fetch distinct="false" mapping="logical" output-format="xml-platform" version="1.0">
			<entity name="account">
				<attribute name="name"/>
				<attribute name="accountnumber"/>
				<attribute name="ownerid"/>
				<attribute name="address1_postalcode"/>
				<attribute name="accountid"/>
				<order descending="false" attribute="name"/>
				<order descending="false" attribute="accountnumber"/>
			</entity>
		</fetch>'
	);

	var_dump($controller->findAll($fetchXML));
